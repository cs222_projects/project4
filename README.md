# README #

This is the 4th project of the course  [Principles of Data Management (cs222)](https://grape.ics.uci.edu/wiki/asterix/wiki/cs222-2014-fall) in UC Irvine. This series of project aims to build a database management system, which include storage management, buffer management, record-oriented file systems, access methods, query optimization, and query processing.


### Introduction ###

Directory structure and contents:

| Directory                  | Purpose                                                                       |
|----------------------------|-------------------------------------------------------------------------------|
| `rbf/`       | Record-based File Layer                    |
| `ix/`         | Index Layer                                                 |
| `rm/`       | Relation Management Layer                                                           |
| `qe/`       | Query Execution Layer.                                        |
| `cli/`        | Command Line Layer                                        |
| `data/`    | Data for running test cases                                                    |

### Other Projects of This Course ###

* [project 3](https://bitbucket.org/cs222_projects/project3)

* [project 2](https://bitbucket.org/cs222_projects/project2)

* [project 1 (Wat's version)](https://bitbucket.org/cs222_projects/project1_oliver)

* [project 1 (Xiang's version)](https://bitbucket.org/cs222_projects/project1_xiangyan)

### Set Up ###

Note that currently this project has only been tested on Ubuntu 14.04, while compiled by gcc 4.9.

```
#!shell

cd codebase
cd cli
make
./start
```