
#ifndef _rm_h_
#define _rm_h_

#include <string.h>
#include <vector>
#include <map>
#include <stdlib.h>

// #include "../rbf/rbfm.h"
#include "../ix/ix.h"

using namespace std;

#define TABLE_FILE_NAME "Tables"
#define COLUMN_FILE_NAME "Columns"
#define INDEX_FILE_NAME "Indexes"
#define FILE_NAME_LEN 255
#define TABLE_NAME_LEN 255
#define COLUMN_NAME_LEN 255
#define INDEX_NAME_LEN 255

#define MAX_TUPLE_SIZE MAX_RECORD_SIZE

# define RM_EOF (-1)  // end of a scan operator


// RM_ScanIterator is an iteratr to go through tuples
// The way to use it is like the following:
//  RM_ScanIterator rmScanIterator;
//  rm.open(..., rmScanIterator);
//  while (rmScanIterator(rid, data) != RM_EOF) {
//    process the data;
//  }
//  rmScanIterator.close();

class RM_ScanIterator {
public:
  RM_ScanIterator() {};
  ~RM_ScanIterator() {};

  // "data" follows the same format as RelationManager::insertTuple()
  RC getNextTuple(RID &rid, void *data);
  RC close();
  RC setRBFMSI(RBFM_ScanIterator* _rbfmsi);
    
private:
    RBFM_ScanIterator* _rbfmsi;
};


class RM_IndexScanIterator {
 public:
  RM_IndexScanIterator();   // Constructor
  ~RM_IndexScanIterator();  // Destructor

  // "key" follows the same format as in IndexManager::insertEntry()
  RC getNextEntry(RID &rid, void *key);   // Get next matching entry
  RC close();                   // Terminate index scan
  RC setIXSI(IX_ScanIterator* _ixsi);
 private:
  IX_ScanIterator* _ixsi;
};


// Relation Manager
class RelationManager
{
public:
  static RelationManager* instance();

  RC createTable(const string &tableName, const vector<Attribute> &attrs);

  RC deleteTable(const string &tableName);

  RC getAttributes(const string &tableName, vector<Attribute> &attrs);

  RC insertTuple(const string &tableName, const void *data, RID &rid);

  RC deleteTuples(const string &tableName);

  RC deleteTuple(const string &tableName, const RID &rid);

  // Assume the rid does not change after update
  RC updateTuple(const string &tableName, const void *data, const RID &rid);

  RC readTuple(const string &tableName, const RID &rid, void *data);

  RC readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data);

  RC reorganizePage(const string &tableName, const unsigned pageNumber);

  // scan returns an iterator to allow the caller to go through the results one by one. 
  RC scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,                  // comparision type such as "<" and "="
      const void *value,                    // used in the comparison
      const vector<string> &attributeNames, // a list of projected attributes
      RM_ScanIterator &rm_ScanIterator);


// Extra credit
public:
  RC dropAttribute(const string &tableName, const string &attributeName);

  RC addAttribute(const string &tableName, const Attribute &attr);

  RC reorganizeTable(const string &tableName);

public:

  RC createIndex(const string &tableName, const string &attributeName);

  RC destroyIndex(const string &tableName, const string &attributeName);

  // indexScan returns an iterator to allow the caller to go through qualified entries in index
  RC indexScan(const string &tableName,
                        const string &attributeName,
                        const void *lowKey,
                        const void *highKey,
                        bool lowKeyInclusive,
                        bool highKeyInclusive,
                        RM_IndexScanIterator &rm_IndexScanIterator
       );



protected:
  RelationManager();
  ~RelationManager();

private:
    RC initTableMap();
    RC initIndexMap();
    
    RC getTableDescription(vector<Attribute> &recordDescriptor);
    RC getAttributeDescription(vector<Attribute> &recordDescriptor);
    RC getIndexDescription(vector<Attribute> &indexDescriptor);
    
    RC tableIdToRid(int tableId, RID &rid);
    RC getContentFileName(const string &tableName, string &fileName, bool isSystemTable);
    RC checkTableName(const string &tableName);
    int getSizeOfdata(vector<Attribute> &attr, void* data);
    RC getAttributesSelectively(const string &tableName, vector<Attribute> &attrs, bool needAll);
    RC doCreateTable(const string &tableName, const vector<Attribute> &attrs, bool fromOuter);
    
    string getIndexName(const string &tableName, const string &attributeName);
    string getIndexMapKey(const int tableId, const string columnName);
    RC getOneAttributeFromName(const string &tableName, const string &attributeName, Attribute &attr);
    
    RC insertIndex(const string &tableName, const void *data, const RID &rid);
    RC deleteIndex(const string &tableName, const void *data, const RID &rid);
    RC searchExistedIndex(const string &tableName, vector<Attribute> &attribute, vector<Attribute> &indexedAttr,
                          const void *data, vector<void *> &keys);
    RC searchExistedIndexWithoutKey(const string &tableName, vector<Attribute> &attribute, vector<Attribute> &indexedAttr);
    
    std::map<string, int> _tableMap;
    std::map<int, vector<Attribute> > _columnsMap;
    std::map<string, bool> _indexMap;
    
    RecordBasedFileManager* _rbfm;
    IndexManager* _ix;
    
  static RelationManager *_rm;
};

#endif
