
#include "rm.h"

RelationManager* RelationManager::_rm = 0;

RelationManager* RelationManager::instance()
{
    if(!_rm)
        _rm = new RelationManager();

    return _rm;
}

RelationManager::RelationManager()
{
    _rbfm = RecordBasedFileManager::instance();
    _ix = IndexManager::instance();
    FileHandle fileHandle;
    if(_rbfm->openFile(TABLE_FILE_NAME, fileHandle) != 0 || _rbfm->openFile(COLUMN_FILE_NAME, fileHandle) != 0
       || _rbfm->openFile(INDEX_FILE_NAME, fileHandle) != 0)
    {
        // If the table catalog hasn't been created
        _rbfm->createFile(TABLE_FILE_NAME);
        _rbfm->createFile(COLUMN_FILE_NAME);
        _rbfm->createFile(INDEX_FILE_NAME);
        vector<Attribute> tableDesc;
        vector<Attribute> attributeDesc;
        getTableDescription(tableDesc);
        getAttributeDescription(attributeDesc);
        doCreateTable(TABLE_FILE_NAME, tableDesc, false);
        doCreateTable(COLUMN_FILE_NAME, attributeDesc, false);
    }else
    {
        _rbfm->closeFile(fileHandle);
    }
    if(initTableMap() != 0)
    {
        perror("rm constructor");
    }
    if(initIndexMap() != 0)
    {
        perror("rm constructor");
    }
}

RelationManager::~RelationManager()
{
    if(_rm)
    {
        delete _rm;
        _rm = 0;
    }
}

RC RelationManager::createTable(const string &tableName, const vector<Attribute> &attrs)
{
    return doCreateTable(tableName, attrs, true);
}

RC RelationManager::deleteTable(const string &tableName)
{
    // ----------------------
    // Delete table data file
    // ----------------------
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    int tableId = _tableMap[tableName];
    if(tableId == 0)
    {
        return -1;
    }
    if(_rbfm->destroyFile(tableName) != 0)
    {
        return -1;
    }
    // ------------
    // Delete index
    // ------------
    vector<Attribute> attrs;
    if(getAttributes(tableName, attrs) != 0)
    {
        return -1;
    }
    for(vector<Attribute>::const_iterator it = attrs.begin(); it != attrs.end(); it++)
    {
        string indexMapKey = getIndexMapKey(tableId, it->name);
        if(!_indexMap[indexMapKey])
        {
            _indexMap.erase(indexMapKey);
            continue;
        }
        destroyIndex(tableName, it->name);
    }
    // -----------------
    // Delete attributes
    // -----------------
    RM_ScanIterator rmsi;
    vector<string> projectedAttr;
    projectedAttr.push_back("column-name");
    scan(COLUMN_FILE_NAME, "table-id", EQ_OP, &tableId, projectedAttr, rmsi);
    RID rid;
    char returnedData[PAGE_SIZE];
    FileHandle fileHandle;
    if(_rbfm->openFile(COLUMN_FILE_NAME, fileHandle) != 0)
    {
        return -1;
    }
    while(rmsi.getNextTuple(rid, returnedData) != RM_EOF)
    {
        if(_rbfm->deleteRecord(fileHandle, attrs, rid) != 0)
        {
            return -1;
        }
    }
    _columnsMap.erase(tableId);
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    // ------------
    // Delete table
    // ------------
    RID tableRid;
    tableRid.pageNum = (tableId-1)/NUM_OF_SLOTS;
    tableRid.slotNum = (tableId-1)%NUM_OF_SLOTS;
    FileHandle tableFileHandle;
    if(_rbfm->openFile(TABLE_FILE_NAME, tableFileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> tableAttr;
    getTableDescription(tableAttr);
    if(_rbfm->deleteRecord(tableFileHandle, tableAttr, tableRid) != 0)
    {
        return -1;
    }
    _tableMap.erase(tableName);
    if(_rbfm->closeFile(tableFileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::getAttributes(const string &tableName, vector<Attribute> &attrs)
{
    return getAttributesSelectively(tableName, attrs, false);
}

RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->insertRecord(fileHandle, attr, data, rid) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    if(insertIndex(tableName, data, rid) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::deleteTuples(const string &tableName)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    if(_rbfm->deleteRecords(fileHandle) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    
    vector<Attribute> attribute;
    vector<Attribute> indexedAttr;
    getAttributes(tableName, attribute);
    if(searchExistedIndexWithoutKey(tableName, attribute, indexedAttr) != 0)
    {
        return -1;
    }
    for(vector<Attribute>::const_iterator it = indexedAttr.begin(); it != indexedAttr.end(); it++)
    {
        string indexName = getIndexName(tableName, it->name);
        if(_ix->destroyFile(indexName) != 0)
        {
            return -1;
        }
        if(_ix->createFile(indexName, 1) != 0)
        {
            return -1;
        }
    }
    return 0;
}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    // Read data to delete
    void *data = malloc(MAX_TUPLE_SIZE);
    memset(data, 0, MAX_TUPLE_SIZE);
    if(readTuple(tableName, rid, data) != 0)
    {
        return -1;
    }
    
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->deleteRecord(fileHandle, attr, rid) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    if(deleteIndex(tableName, data, rid) != 0)
    {
        return -1;
    }
    
    free(data);
    return 0;
}

RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    // Read data to delete
    void *oldData = malloc(MAX_TUPLE_SIZE);
    memset(oldData, 0, MAX_TUPLE_SIZE);
    if(readTuple(tableName, rid, oldData) != 0)
    {
        return -1;
    }
    if(deleteIndex(tableName, oldData, rid) != 0)
    {
        return -1;
    }
    free(oldData);
    
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->updateRecord(fileHandle, attr, data, rid) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    if(insertIndex(tableName, data, rid) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::readTuple(const string &tableName, const RID &rid, void *data)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->readRecord(fileHandle, attr, rid, data) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->readAttribute(fileHandle, attr, rid, attributeName, data) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::reorganizePage(const string &tableName, const unsigned pageNumber)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->reorganizePage(fileHandle, attr, pageNumber) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,                  
      const void *value,                    
      const vector<string> &attributeNames,
      RM_ScanIterator &rm_ScanIterator)
{
    string contentFileName;
    getContentFileName(tableName, contentFileName, true);
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        perror("rm->scan: open file");
        return -1;
    }
    vector<Attribute> attr;
    if(contentFileName == TABLE_FILE_NAME)
    {
        getTableDescription(attr);
    }else if(contentFileName == COLUMN_FILE_NAME)
    {
        getAttributeDescription(attr);
    }else if(contentFileName == INDEX_FILE_NAME)
    {
        getIndexDescription(attr);
    }else if(getAttributes(tableName, attr) != 0)
    {
        perror("rm->scan: getAttributes");
        return -1;
    }
    RBFM_ScanIterator* _rbfmsi = new RBFM_ScanIterator();
    if(_rbfm->scan(fileHandle, attr, conditionAttribute, compOp, value, attributeNames, *_rbfmsi) != 0)
    {
        perror("rm->scan: _rbfm->scan");
        return -1;
    }
    rm_ScanIterator.setRBFMSI(_rbfmsi);
    return 0;
}

RC RelationManager::initTableMap()
{
    RM_ScanIterator rmsi;
    vector<string> attrbuteNames;
    attrbuteNames.push_back("table-name");
    attrbuteNames.push_back("table-id");
    if(scan(TABLE_FILE_NAME, "", NO_OP, NULL, attrbuteNames, rmsi) != 0)
    {
        perror("initTableMap->scan");
        return -1;
    }
    RID rid;
    char scanData[PAGE_SIZE];
    while(rmsi.getNextTuple(rid, scanData) != RM_EOF){
        //process the data;
        int stringLen;
        memcpy(&stringLen, scanData, sizeof(int));
        char tableNameChar[stringLen+1];
        memcpy(tableNameChar, scanData+sizeof(int), stringLen);
        tableNameChar[stringLen] = '\0';
        string tableName(tableNameChar);
        int tableId;
        memcpy(&tableId, scanData+sizeof(int)+stringLen, sizeof(int));
        _tableMap[tableName] = tableId;
    }
    rmsi.close();
    return 0;
}

RC RelationManager::initIndexMap()
{
    RM_ScanIterator rmsi;
    vector<string> attributeNames;
    attributeNames.push_back("table-id");
    attributeNames.push_back("column-name");
    if(scan(INDEX_FILE_NAME, "", NO_OP, NULL, attributeNames, rmsi) != 0)
    {
        perror("initIndexMap->scan");
        return -1;
    }
    RID rid;
    char scanData[PAGE_SIZE];
    while (rmsi.getNextTuple(rid, scanData) != RM_EOF) {
        // process the data
        int tableId;
        memcpy(&tableId, scanData, sizeof(int));
        int stringLen;
        memcpy(&stringLen, scanData+sizeof(int), sizeof(int));
        char columnNameChar[stringLen+1];
        memcpy(columnNameChar, scanData+2*sizeof(int), stringLen);
        columnNameChar[stringLen] = '\0';
        string columnName(columnNameChar);
        _indexMap[getIndexMapKey(tableId, columnName)] = true;
    }
    rmsi.close();
    return 0;
}

string RelationManager::getIndexMapKey(const int tableId, const string columnName)
{
    return to_string(tableId) + "_" + columnName;
}

RC RelationManager::getTableDescription(vector<Attribute> &recordDescriptor)
{
    Attribute tableId, tableName, fileName;
//    int tableFlag;
    tableId.name = "table-id";
    tableId.type = TypeInt;
    tableId.length = sizeof(int);
    recordDescriptor.push_back(tableId);
    tableName.name = "table-name";
    tableName.type = TypeVarChar;
    tableName.length = TABLE_NAME_LEN;
    recordDescriptor.push_back(tableName);
    fileName.name = "file-name";
    fileName.type = TypeVarChar;
    fileName.length = FILE_NAME_LEN;
    recordDescriptor.push_back(fileName);
    return 0;
}

RC RelationManager::getAttributeDescription(vector<Attribute> &recordDescriptor)
{
    Attribute descTableId, descName, descType, descLength, descColumnId, isExisted;
    recordDescriptor.clear();
    descTableId.name = "table-id";
    descTableId.type = TypeInt;
    descTableId.length = sizeof(int);
    recordDescriptor.push_back(descTableId);
    descName.name = "column-name";
    descName.type = TypeVarChar;
    descName.length = COLUMN_NAME_LEN;
    recordDescriptor.push_back(descName);
    descType.name = "column-type";
    descType.type = TypeInt;
    descType.length = sizeof(TypeInt);
    recordDescriptor.push_back(descType);
    descLength.name = "column-length";
    descLength.type = TypeInt;
    descLength.length = sizeof(int);
    recordDescriptor.push_back(descLength);
    descColumnId.name = "column-id";
    descColumnId.type = TypeInt;
    descColumnId.length = sizeof(int);
    recordDescriptor.push_back(descColumnId);
    isExisted.name = "is-existed";
    isExisted.type = TypeInt;
    isExisted.length = sizeof(int);
    recordDescriptor.push_back(isExisted);
    return 0;
}

RC RelationManager::getIndexDescription(vector<Attribute> &indexDescriptor)
{
    Attribute tableId, columnName, indexName;
    tableId.name = "table-id";
    tableId.type = TypeInt;
    tableId.length = sizeof(int);
    indexDescriptor.push_back(tableId);
    columnName.name = "column-name";
    columnName.type = TypeVarChar;
    columnName.length = COLUMN_NAME_LEN;
    indexDescriptor.push_back(columnName);
    indexName.name = "index-name";
    indexName.type = TypeVarChar;
    indexName.length = INDEX_NAME_LEN;
    indexDescriptor.push_back(indexName);
    return 0;
}


RC RelationManager::getContentFileName(const string &tableName, string &fileName, bool isSystemTable)
{
    // Simple method, can be flexible
    if(!isSystemTable && checkTableName(tableName) != 0)
    {
        return -1;
    }
    fileName = tableName;
    return 0;
}

RC RelationManager::checkTableName(const string &tableName)
{
    if(tableName == TABLE_FILE_NAME || tableName == COLUMN_FILE_NAME || tableName == INDEX_FILE_NAME)
    {
        return -1;
    }
    return 0;
}


int RelationManager::getSizeOfdata(vector<Attribute> &attr, void* data)
{
    int offset = 0;
    for(int i = 0; i < (int)attr.size(); i++)
    {
        if(attr[i].type == TypeVarChar)
        {
            int stringLen = 0;
            memcpy(&stringLen, (char*)data + offset, sizeof(int));
            offset += stringLen;
            offset += sizeof(int);
        } else
        {
            offset += sizeof(int);
        }
    }
    return offset;
}
                          

RC RelationManager::getAttributesSelectively(const string &tableName, vector<Attribute> &attrs, bool needAll)
{
    if(tableName == TABLE_FILE_NAME)
    {
        return getTableDescription(attrs);
    }else if(tableName == COLUMN_FILE_NAME)
    {
        return getAttributeDescription(attrs);
    }else if(tableName == INDEX_FILE_NAME)
    {
        return getIndexDescription(attrs);
    }
    int tableId = _tableMap[tableName];
    if(tableId == 0)
    {
        perror("rm->getAttributes: tableId == 0");
        return -1;
    }
    vector<Attribute> emptyAttr;
    if(memcmp(&_columnsMap[tableId], &emptyAttr, sizeof(vector<Attribute>)) != 0)
    {
        if(!needAll)
        {
            attrs = _columnsMap[tableId];
            return 0;
        }
    }
    // Search columns according to table id
    _columnsMap.erase(tableId);
    RM_ScanIterator rmsi;
    vector<string> projectedAttr;
    projectedAttr.push_back("column-name");
    projectedAttr.push_back("column-type");
    projectedAttr.push_back("column-length");
    projectedAttr.push_back("column-id");
    projectedAttr.push_back("is-existed");
    scan(COLUMN_FILE_NAME, "table-id", EQ_OP, &tableId, projectedAttr, rmsi);
    RID rid;
    char returnedData[MAX_TUPLE_SIZE];
    std::map<int, Attribute> attrMap; // Arrange to get the correct order
    while(rmsi.getNextTuple(rid, returnedData) != RM_EOF)
    {
        Attribute attribute;
        int columnId, isExisted;
        AttrType columnType;
        AttrLength columnLength;
        int offset = 0, columnNameLength = 0;
        memcpy(&columnNameLength, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        char columnNameCharArray[columnNameLength+1];
        memcpy(&columnNameCharArray, returnedData+offset, columnNameLength);
        columnNameCharArray[columnNameLength] = '\0';
        string columnName(columnNameCharArray);
        offset += columnNameLength;
        memcpy(&columnType, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnLength, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnId, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&isExisted, returnedData+offset, sizeof(int));
        if(isExisted == 0 && !needAll)
        {
            continue;
        }
        attribute.name = columnName;
        attribute.type = columnType;
        attribute.length = columnLength;
        attrMap[columnId] = attribute;
    }
    std::map<int, Attribute>::iterator it;
    for(it = attrMap.begin(); it != attrMap.end(); ++it)
    {
        attrs.push_back(it->second);
    }
    if(!needAll)
    {
        _columnsMap[tableId] = attrs;
    }
    rmsi.close();
    return 0;
}

RC RelationManager::doCreateTable(const string &tableName, const vector<Attribute> &attrs, bool fromOuter)
{
    // -----------------------
    // Fisrt create table info
    // -----------------------
    if(fromOuter && checkTableName(tableName) != 0)
    {
        return -1;
    }
    if(fromOuter && _tableMap[tableName] != 0)
    {
        return -1;
    }
    _tableMap.erase(tableName);
    
    vector<Attribute> tableInfo;
    getTableDescription(tableInfo);
    
    int dataLength = 0, tableNameLength = 0;
    tableNameLength = tableName.length();
    dataLength += 4 * sizeof(int);
    dataLength += 2 * tableNameLength;
    void* data = malloc(dataLength);
    int offset = 0;
    //    int userFlag = 1;
    int tableId = 0;
    memset(data, 0, dataLength);
    memcpy((char *)data + offset, &tableId, sizeof(int));
    offset += sizeof(int);
    memcpy((char *)data + offset, &tableNameLength, sizeof(int));
    offset += sizeof(int);
    memcpy((char *)data + offset, tableName.c_str(), tableName.length());
    offset += tableName.length();
    memcpy((char *)data + offset, &tableNameLength, sizeof(int));
    offset += sizeof(int);
    memcpy((char *)data + offset, tableName.c_str(), tableName.length());
    offset += tableName.length();
    
    RID rid;
    FileHandle fileHandle;
    if(_rbfm->openFile(TABLE_FILE_NAME, fileHandle) != 0)
    {
        free(data);
        return -1;
    }
    if(_rbfm->insertRecord(fileHandle, tableInfo, data, rid) != 0)
    {
        perror("rm->createTable: insertRecord");
        free(data);
        return -1;
    }
    
    // table-id is calculated by rid
    int tableIdTrue = rid.pageNum * NUM_OF_SLOTS + rid.slotNum + 1;
    memcpy(data, &tableIdTrue, sizeof(int));
    if(_rbfm->updateRecord(fileHandle, tableInfo, data, rid) != 0)
    {
        perror("rm->createTable: updateRecord");
        free(data);
        return -1;
    }
    free(data);
    _tableMap[tableName] = tableIdTrue;
    
    // ----------------------
    // Create attributes info
    // ----------------------
    int columnIndexCount = 1;
    RID descRid;
    FileHandle columnFileHandle;
    if(_rbfm->openFile(COLUMN_FILE_NAME, columnFileHandle) != 0)
    {
        return -1;
    }
    for(vector<Attribute>::const_iterator it = attrs.begin(); it != attrs.end(); it++)
    {
        vector<Attribute> columnInfo;
        getAttributeDescription(columnInfo);
        
        int attrDataLength = 0, attrNameLength = 0, isExisted = 1;
        attrNameLength = it->name.length();
        attrDataLength = 6 * sizeof(int);
        attrDataLength += attrNameLength;
        void* attrData = malloc(attrDataLength);
        memset(attrData, 0, attrDataLength);
        int descOffset = 0;
        memcpy((char *)attrData + descOffset, &tableIdTrue, sizeof(int));
        descOffset += sizeof(int);
        memcpy((char *)attrData + descOffset, &attrNameLength, sizeof(int));
        descOffset += sizeof(int);
        memcpy((char *)attrData + descOffset, (it->name).c_str(), attrNameLength);
        descOffset += attrNameLength;
        memcpy((char *)attrData + descOffset, &(it->type), sizeof(TypeInt));
        descOffset += sizeof(TypeInt);
        memcpy((char *)attrData + descOffset, &(it->length), sizeof(int));
        descOffset += sizeof(int);
        memcpy((char *)attrData + descOffset, &columnIndexCount, sizeof(int));
        columnIndexCount++;
        descOffset += sizeof(int);
        memcpy((char *)attrData + descOffset, &isExisted, sizeof(int));

        if(_rbfm->insertRecord(columnFileHandle, columnInfo, attrData, descRid) != 0)
        {
            free(attrData);
            return -1;
        }

        free(attrData);
    }
    _columnsMap[tableIdTrue] = attrs;
    
    // Create data file
    if(_rbfm->createFile(tableName) != 0)
    {
        return -1;
    }
    return 0;
}
    
// Extra credit
RC RelationManager::dropAttribute(const string &tableName, const string &attributeName)
{
    return -1;
}

// Extra credit
RC RelationManager::addAttribute(const string &tableName, const Attribute &attr)
{
    return -1;
}

// Extra credit
RC RelationManager::reorganizeTable(const string &tableName)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->reorganizeFile(fileHandle, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::createIndex(const string &tableName, const string &attributeName)
{
    // Shit code!
    if(checkTableName(tableName) != 0)
    {
        return -1;
    }
    int tableId = _tableMap[tableName];
    if(tableId == 0)
    {
        return -1;
    }
    string indexName = getIndexName(tableName, attributeName);
    string indexMapKey = getIndexMapKey(tableId, attributeName);
    if(_indexMap[indexMapKey])
    {
        return -1;
    }
    _indexMap.erase(indexMapKey);
    
    // Create index file
    if(_ix->createFile(indexName, 1) != 0)
    {
        return -1;
    }
    IXFileHandle ixfileHandle;
    if(_ix->openFile(indexName, ixfileHandle) != 0)
    {
        return -1;
    }
    RM_ScanIterator rmsi;
    vector<string> projectedAttr;
    projectedAttr.push_back(attributeName);
    scan(tableName, "", NO_OP, NULL, projectedAttr, rmsi);
    RID rid;
    char returnedData[MAX_TUPLE_SIZE];
    Attribute attr;
    if(getOneAttributeFromName(tableName, attributeName, attr) != 0)
    {
        return -1;
    }
    while(rmsi.getNextTuple(rid, returnedData) != RM_EOF)
    {
        if(_ix->insertEntry(ixfileHandle, attr, returnedData, rid) != 0)
        {
            return -1;
        }
    }
    if(_ix->closeFile(ixfileHandle) != 0)
    {
        return -1;
    }
    
    
    vector<Attribute> indexInfo;
    getIndexDescription(indexInfo);
    
    int dataLength = 0;
    int attributeNameLength = attributeName.length();
    int indexNameLength = indexName.length();
    dataLength = 3*sizeof(int) + attributeNameLength + indexNameLength;
    void* data = malloc(dataLength);
    memset(data, 0, dataLength);
    
    int offset = 0;
    memcpy((char *)data + offset, &tableId, sizeof(int));
    offset += sizeof(int);
    memcpy((char *)data + offset, &attributeNameLength, sizeof(int));
    offset += sizeof(int);
    memcpy((char *)data + offset, attributeName.c_str(), attributeName.length());
    offset += attributeName.length();
    memcpy((char *)data + offset, &indexNameLength, sizeof(int));
    offset += sizeof(int);
    memcpy((char *)data + offset, indexName.c_str(), indexName.length());
    offset += indexName.length();
    
    RID indexFileRid;
    FileHandle fileHandle;
    if(_rbfm->openFile(INDEX_FILE_NAME, fileHandle) != 0)
    {
        free(data);
        return -1;
    }
    if(_rbfm->insertRecord(fileHandle, indexInfo, data, indexFileRid) != 0)
    {
        perror("rm->createIndex: insertRecord");
        free(data);
        return -1;
    }
    free(data);
    _indexMap[indexMapKey] = true;
    
    return 0;
}

RC RelationManager::destroyIndex(const string &tableName, const string &attributeName)
{
    // Delete catalog info
    if(checkTableName(tableName) != 0)
    {
        return -1;
    }
    int tableId = _tableMap[tableName];
    if(tableId == 0)
    {
        return -1;
    }
    string indexName = getIndexName(tableName, attributeName);
    string indexMapKey = getIndexMapKey(tableId, attributeName);
    if(!_indexMap[indexMapKey])
    {
        _indexMap.erase(indexMapKey);
        return -1;
    }
    
    RM_ScanIterator rmsi;
    vector<string> projectedAttr;
    projectedAttr.push_back("index-name");

    int indexNameLength = indexName.length();
    void *indexNameData = malloc(indexNameLength+sizeof(int));
    memcpy(indexNameData, &indexNameLength, sizeof(int));
    memcpy((char *)indexNameData + sizeof(int), indexName.c_str(), indexNameLength);
    scan(INDEX_FILE_NAME, "index-name", EQ_OP, indexNameData, projectedAttr, rmsi);
    free(indexNameData);
    
    RID rid;
    char returnedData[MAX_TUPLE_SIZE];
    FileHandle fileHandle;
    vector<Attribute> attrs;
    if(getIndexDescription(attrs) != 0)
    {
        return -1;
    }
    if(_rbfm->openFile(INDEX_FILE_NAME, fileHandle) != 0)
    {
        return -1;
    }
    while(rmsi.getNextTuple(rid, returnedData) != RM_EOF)
    {
        if(_rbfm->deleteRecord(fileHandle, attrs, rid) != 0)
        {
            return -1;
        }
        break;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    
    // Delete index file
    if(_ix->destroyFile(indexName) != 0)
    {
        return -1;
    }
    
    _indexMap.erase(indexMapKey);
    
    return 0;
}

  // indexScan returns an iterator to allow the caller to go through qualified entries in index
RC RelationManager::indexScan(const string &tableName,
                        const string &attributeName,
                        const void *lowKey,
                        const void *highKey,
                        bool lowKeyInclusive,
                        bool highKeyInclusive,
                        RM_IndexScanIterator &rm_IndexScanIterator)
{
    if(checkTableName(tableName) != 0)
    {
        return -1;
    }
    int tableId = _tableMap[tableName];
    if(tableId == 0)
    {
        return -1;
    }
    string indexName = getIndexName(tableName, attributeName);
    string indexMapKey = getIndexMapKey(tableId, attributeName);
    if(!_indexMap[indexMapKey])
    {
        return -1;
    }
    
    IXFileHandle ixfileHandle;
    if(_ix->openFile(indexName, ixfileHandle) != 0)
    {
        return -1;
    }
    Attribute attr;
    if(getOneAttributeFromName(tableName, attributeName, attr) != 0)
    {
        return -1;
    }
    IX_ScanIterator* ix_ScanIterator = new IX_ScanIterator();
    if(_ix->scan(ixfileHandle, attr, lowKey, highKey, lowKeyInclusive, highKeyInclusive, *ix_ScanIterator))
    {
        return -1;
    }
    rm_IndexScanIterator.setIXSI(ix_ScanIterator);
    
    return 0;
}

string RelationManager::getIndexName(const string &tableName, const string &attributeName)
{
    return tableName + "." + attributeName;
}

RC RelationManager::getOneAttributeFromName(const string &tableName, const string &attributeName, Attribute &attr)
{
    vector<Attribute> attrs;
    if(getAttributes(tableName, attrs) != 0)
    {
        return -1;
    }
    for(vector<Attribute>::const_iterator it = attrs.begin(); it != attrs.end(); it++)
    {
        if(it->name == attributeName)
        {
            attr = *it;
            return 0;
        }
    }
    return -1;
}

RC RelationManager::insertIndex(const string &tableName, const void *data, const RID &rid)
{
    vector<Attribute> attribute;
    vector<Attribute> indexedAttr;
    vector<void *> keys;
    if(getAttributes(tableName, attribute) != 0)
    {
        return -1;
    }
    if(searchExistedIndex(tableName, attribute, indexedAttr, data, keys) != 0)
    {
        return -1;
    }
    if(indexedAttr.size() == 0)
    {
        return 0;
    }
    for(int i = 0; i < (int)indexedAttr.size(); i++)
    {
        IXFileHandle ixfileHandle;
        string indexName = getIndexName(tableName, indexedAttr.at(i).name);
        if(_ix->openFile(indexName, ixfileHandle) != 0)
        {
            return -1;
        }
        if(_ix->insertEntry(ixfileHandle, indexedAttr.at(i), keys.at(i), rid) != 0)
        {
            return -1;
        }
        if(_ix->closeFile(ixfileHandle) != 0)
        {
            return -1;
        }
        free(keys.at(i));
    }
    return 0;
}

RC RelationManager::deleteIndex(const string &tableName, const void *data, const RID &rid)
{
    vector<Attribute> attribute;
    vector<Attribute> indexedAttr;
    vector<void *> keys;
    if(getAttributes(tableName, attribute) != 0)
    {
        return -1;
    }
    if(searchExistedIndex(tableName, attribute, indexedAttr, data, keys) != 0)
    {
        return -1;
    }
    if(indexedAttr.size() == 0)
    {
        return 0;
    }
    for(int i = 0; i < (int)indexedAttr.size(); i++)
    {
        IXFileHandle ixfileHandle;
        string indexName = getIndexName(tableName, indexedAttr.at(i).name);
        if(_ix->openFile(indexName, ixfileHandle) != 0)
        {
            return -1;
        }
        if(_ix->deleteEntry(ixfileHandle, indexedAttr.at(i), keys.at(i), rid) != 0)
        {
            return -1;
        }
        if(_ix->closeFile(ixfileHandle) != 0)
        {
            return -1;
        }
        free(keys.at(i));
    }
    return 0;
}

RC RelationManager::searchExistedIndex(const string &tableName, vector<Attribute> &attribute, vector<Attribute> &indexedAttr,
                      const void *data, vector<void *> &keys)
{
    indexedAttr.clear();
    int tableId = _tableMap[tableName];
    int offset = 0;
    for(vector<Attribute>::const_iterator it = attribute.begin(); it != attribute.end(); it++)
    {
        int offsetMove = sizeof(int);
        if(it->type == TypeVarChar)
        {
            int stringLen;
            memcpy(&stringLen, (char *)data + offset, sizeof(int));
            offsetMove += stringLen;
        }
        string indexMapKey = getIndexMapKey(tableId, it->name);
        if(!_indexMap[indexMapKey])
        {
            _indexMap.erase(indexMapKey);
            offset += offsetMove;
            continue;
        }
        indexedAttr.push_back(*it);
        void *key = malloc(offsetMove);
        memset(key, 0, offsetMove);
        memcpy(key, (char *)data + offset, offsetMove);
        keys.push_back(key);
        offset += offsetMove;
    }
    return 0;
}

RC RelationManager::searchExistedIndexWithoutKey(const string &tableName, vector<Attribute> &attribute, vector<Attribute> &indexedAttr)
{
    indexedAttr.clear();
    int tableId = _tableMap[tableName];
    for(vector<Attribute>::const_iterator it = attribute.begin(); it != attribute.end(); it++)
    {
        string indexMapKey = getIndexMapKey(tableId, it->name);
        if(!_indexMap[indexMapKey])
        {
            _indexMap.erase(indexMapKey);
            continue;
        }
    }
    return 0;
}

RC RM_ScanIterator::getNextTuple(RID &rid, void *data)
{
    if(_rbfmsi->getNextRecord(rid, data) != RBFM_EOF)
    {
        return 0;
    } else
    {
        return RM_EOF;
    }
}

RC RM_ScanIterator::close()
{
    if(_rbfmsi != NULL)
    {
        _rbfmsi->close();
        delete _rbfmsi;
        _rbfmsi = 0;
    }
    return 0;
}

RC RM_ScanIterator::setRBFMSI(RBFM_ScanIterator* _rbfmsi)
{
    this->_rbfmsi = _rbfmsi;
    return 0;
}


RM_IndexScanIterator::RM_IndexScanIterator()
{

}

RM_IndexScanIterator::~RM_IndexScanIterator()
{
    
}

RC RM_IndexScanIterator::getNextEntry(RID &rid, void *key)
{
    if(_ixsi->getNextEntry(rid, key) != 0)
    {
        return -1;
    }
    return 0;
}

RC RM_IndexScanIterator::setIXSI(IX_ScanIterator* _ixsi)
{
    this->_ixsi = _ixsi;
    return 0;
}

RC RM_IndexScanIterator::close()
{
    if(_ixsi != NULL)
    {
        _ixsi->close();
        delete _ixsi;
        _ixsi = 0;
    }
    return 0;
}