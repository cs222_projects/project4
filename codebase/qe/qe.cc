
#include <sstream>

#include "qe.h"

// ================
// ==== Filter ====
// ================

Filter::Filter(Iterator* input, const Condition &condition)
{
	this->input = input;
	this->condition = condition;
}

RC Filter::getNextTuple(void *data)
{
	do
	{
		if(input->getNextTuple(data) == QE_EOF)
		{
			return QE_EOF;
		}
	}
	while(isSatisfied(data) == false);

	return 0;
}

void Filter::getAttributes(vector<Attribute> &attrs) const
{
	input->getAttributes(attrs);
}

bool Filter::isSatisfied(void *data)
{
	bool result = false;

	vector<Attribute> attributes;
	input->getAttributes(attributes);

	//Get left-hand side attribute
	void *lhsAttrData = malloc(MAX_ATTRIBUTE_SIZE);
	memset(lhsAttrData, 0, MAX_ATTRIBUTE_SIZE);
	if(getAttribute(attributes, data, lhsAttrData, condition.lhsAttr, attrType) == -1)
	{
		free(lhsAttrData);
		return false;
	}

	if(condition.bRhsIsAttr == true)
	{
		//Get right-hand side attribute
		void *rhsAttrData = malloc(MAX_ATTRIBUTE_SIZE);
		memset(rhsAttrData, 0, MAX_ATTRIBUTE_SIZE);

		if(getAttribute(attributes, data, rhsAttrData, condition.rhsAttr, attrType) == -1)
		{
			free(lhsAttrData);
			free(rhsAttrData);
			return false;
		}
		
		//Compare with right-hand side attribute
		result = compValue(lhsAttrData, rhsAttrData);

		free(rhsAttrData);
	}
	else
	{
		//Compare with right-hand side value
		result = compValue(lhsAttrData, condition.rhsValue.data);
	}

	//Free memory
	free(lhsAttrData);

	return result;
}

bool Filter::compValue(const void *leftData, const void *rightData)
{
	if(attrType == TypeInt)
    {
        return compNumValue<int>(*(int *)leftData, *(int *)rightData, condition.op);
    }

    if(attrType == TypeReal)
    {
        return compNumValue<float>(*(float *)leftData, *(float *)rightData, condition.op);
    }

    if(attrType == TypeVarChar)
    {
        return compStrValue(((char *)leftData + sizeof(int)), ((char *)rightData + sizeof(int)), condition.op);
    }

    return false;
}


// ================
// ==== Project ===
// ================

Project::Project(Iterator *input, const vector<string> &attrNames):_itr(input)
{
    _itr->getAttributes(_allAttrs);
    for(unsigned i = 0; i < attrNames.size(); i++)
    {
        for(vector<Attribute>::const_iterator it = _allAttrs.begin(); it != _allAttrs.end(); it++)
        {
            if(it->name == attrNames.at(i))
            {
                _projectedAttrs.push_back(*it);
                break;
            }
        }
    }
}

RC Project::getNextTuple(void *data)
{
    void *tuple = malloc(PAGE_SIZE);
    memset(tuple, 0, PAGE_SIZE);
    if(_itr->getNextTuple(tuple) != 0)
    {
        free(tuple);
        return QE_EOF;
    }
    
    int projectedAttrsOffset = 0;
    for(unsigned i = 0; i < _projectedAttrs.size(); i++)
    {
        int allAttrsOffset = 0;
        for(vector<Attribute>::const_iterator it = _allAttrs.begin(); it != _allAttrs.end(); it++)
        {
            int allOffsetMoved = sizeof(int);
            if(it->type == TypeVarChar)
            {
                int stringLen = 0;
                memcpy(&stringLen, (char *)tuple + allAttrsOffset, sizeof(int));
                allOffsetMoved += stringLen;
            }
            if(it->name == _projectedAttrs.at(i).name)
            {
                memcpy((char *)data + projectedAttrsOffset, (char *)tuple + allAttrsOffset, allOffsetMoved);
                projectedAttrsOffset += allOffsetMoved;
                break;
            }
            allAttrsOffset += allOffsetMoved;
        }
    }
    
    free(tuple);
    return 0;
}

void Project::getAttributes(vector<Attribute> &attrs) const
{
    attrs.clear();
    attrs = _projectedAttrs;
}


// ================
// === Aggregate ==
// ================

Aggregate::Aggregate(Iterator *input, Attribute aggAttr, AggregateOp op):_itr(input), _aggAttr(aggAttr), _op(op), _getNextEnd(false)
{
    _itr->getAttributes(_allAttrs);
    findAggAttrPos();
    _inputInt = (_aggAttr.type == TypeInt);
}

Aggregate::Aggregate(Iterator *input, Attribute aggAttr, Attribute groupAttr, AggregateOp op, const unsigned numPartitions):_itr(input), _aggAttr(aggAttr), _op(op), _groupAttr(groupAttr), _numPartitions(numPartitions), _getNextEnd(false), _hasGroup(true)
{
    _itr->getAttributes(_allAttrs);
    findAggAttrPos();
    findGroupAttrPos();
    _inputInt = (_aggAttr.type == TypeInt);
    if(initRBFMFiles() != 0)
    {
        cout<<"initialize Group Aggregate rbfm files err"<<endl;
        return;
    }
    if(putDataInFiles() != 0)
    {
        cout<<"put data in files err"<<endl;
        return;
    }
    if(produceTable() != 0)
    {
        cout<<"produce table err"<<endl;
        return;
    }
    _iteratorNum = _aggreMapNum.begin();
    _iteratorStr = _aggreMapStr.begin();
}

Aggregate::~Aggregate()
{
    
}

RC Aggregate::destruct()
{
    bool err = false;
    // Free table
    for (_iteratorNum = _aggreMapNum.begin(); _iteratorNum != _aggreMapNum.end(); ++_iteratorNum)
    {
        free(_iteratorNum->second);
    }
    for (_iteratorStr = _aggreMapStr.begin(); _iteratorStr != _aggreMapStr.end(); ++_iteratorStr)
    {
        free(_iteratorStr->second);
    }
    
    // Delete partition files
    for(unsigned i = 0; i < _numPartitions; i++)
    {
        if(_rbfm->closeFile(*(_fileHandles.at(i))) != 0)
        {
            err = true;
            break;
        }
        if(_rbfm->destroyFile(getPartitionName(i)) != 0)
        {
            err = true;
            break;
        }
    }
    if(err)
    {
        cout<<"~ err"<<endl;
    }
    for(unsigned i = 0; i < _numPartitions; i++)
    {
        delete _fileHandles.at(i);
    }
    return 0;
}

RC Aggregate::findAggAttrPos()
{
    for(unsigned i = 0; i < _allAttrs.size(); i++)
    {
        if(_allAttrs.at(i).name == _aggAttr.name)
        {
            _attrPos = i;
            return 0;
        }
    }
    return -1;
}

RC Aggregate::findGroupAttrPos()
{
    for(unsigned i = 0; i < _allAttrs.size(); i++)
    {
        if(_allAttrs.at(i).name == _groupAttr.name)
        {
            _groupAttrPos = i;
            return 0;
        }
    }
    return -1;
}

RC Aggregate::initRBFMFiles()
{
    _rbfm = RecordBasedFileManager::instance();
    for(unsigned i = 0; i < _numPartitions; i++)
    {
        string fileName = getPartitionName(i);
        FileHandle *fileHandle = new FileHandle();
        if(_rbfm->createFile(fileName)!=0 || _rbfm->openFile(fileName, *fileHandle)!=0)
        {
            return -1;
        }
        _fileHandles.push_back(fileHandle);
    }
    return 0;
}

RC Aggregate::putDataInFiles()
{
    void *tuple = malloc(MAX_ENTRY_SIZE);
    memset(tuple, 0, MAX_ENTRY_SIZE);
    void *key = malloc(MAX_KEY_SIZE);
    memset(key, 0, MAX_KEY_SIZE);
    
    while(_itr->getNextTuple(tuple) != QE_EOF)
    {
        getAggregatedDataFromTuple(tuple, key, true);
        
        unsigned int hashedValue = 0;
        switch (_groupAttr.type)
        {
            case TypeInt:
            {
                int keyValueInt;
                memcpy(&keyValueInt, key, sizeof(int));
                hashedValue = IndexManager::intHash(keyValueInt);
                break;
            }
                
            case TypeReal:
            {
                float keyValueFloat;
                memcpy(&keyValueFloat, key, sizeof(float));
                hashedValue = IndexManager::realHash(keyValueFloat);
                break;
            }
                
            case TypeVarChar:
            {
                int varCharLength;
                memcpy(&varCharLength, key, sizeof(int));
                char str[varCharLength+1];
                memcpy(str, (char *)key+sizeof(int), varCharLength);
                str[varCharLength] = '\0';
                hashedValue = IndexManager::varCharHash(str);
                break;
            }
                
            default:
                break;
        }
        
        unsigned fileNumber = hashedValue % _numPartitions;
        
        // Write data
        string fileName = getPartitionName(fileNumber);
        RID rid;
        if(_rbfm->insertRecord(*(_fileHandles.at(fileNumber)), _allAttrs, tuple, rid) != 0)
        {
            return -1;
        }
    }
    
    free(tuple);
    free(key);
    
    return 0;
}

RC Aggregate::produceTable()
{
    vector<string> attributeNames;
    attributeNames.push_back(_aggAttr.name);
    attributeNames.push_back(_groupAttr.name);
    void *recordData = malloc(MAX_RECORD_SIZE);
    void *aggValue = malloc(sizeof(int));
    RID rid;
    
    for(int i = 0; i < (int)_numPartitions; i++)
    {
        RBFM_ScanIterator rbfmsi;
        
        if(_rbfm->scan(*(_fileHandles.at(i)), _allAttrs, "", NO_OP, NULL, attributeNames, rbfmsi) != 0)
        {
            return -1;
        }
        
        memset(recordData, 0, MAX_RECORD_SIZE);
        while(rbfmsi.getNextRecord(rid, recordData) != RBFM_EOF)
        {
            memcpy(aggValue, recordData, sizeof(int));
            
            if(_groupAttr.type == TypeVarChar)
            {
                int mapKeyLength;
                memcpy(&mapKeyLength, (char *)recordData + sizeof(int), sizeof(int));
                char mapKeyChar[mapKeyLength+1];
                memcpy(&mapKeyChar, (char *)recordData + 2*sizeof(int), mapKeyLength);
                mapKeyChar[mapKeyLength] = '\0';
                string mapKey(mapKeyChar);
                
                if(storeKeyValue<string>(_aggreMapStr, mapKey, aggValue) != 0)
                {
                    return -1;
                }
            }
            else
            {
                float mapKey;
                memcpy(&mapKey, (char *)recordData + sizeof(int), sizeof(float));
                if(storeKeyValue<float>(_aggreMapNum, mapKey, aggValue) != 0)
                {
                    return -1;
                }
            }
            
        }
    }
    
    free(recordData);
    free(aggValue);
    
    return 0;
}

template <typename T>
RC Aggregate::storeKeyValue(std::map<T, void*> &_aggreMap, T mapKey, void *aggValue)
{
    unsigned count = 0;
    switch (_op) {
        case COUNT:
        {
            if(_aggreMap.find(mapKey) == _aggreMap.end())
            {
                void *storedData = malloc(sizeof(int));
                count = 1;
                memcpy(storedData, &count, sizeof(int));
                _aggreMap[mapKey] = storedData;
            }
            else
            {
                void *storedData = _aggreMap[mapKey];
                memcpy(&count, storedData, sizeof(int));
                count++;
                memcpy(storedData, &count, sizeof(int));
                _aggreMap[mapKey] = storedData;
            }
            break;
        }
            
        case MIN:
        case MAX:
        {
            void * storedData = malloc(sizeof(int));
            
            if(_aggreMap.find(mapKey) == _aggreMap.end())
            {
                memcpy(storedData, aggValue, sizeof(int));
                _aggreMap[mapKey] = storedData;
            }
            else
            {
                if(needChange(aggValue, _aggreMap[mapKey]))
                {
                    free(_aggreMap[mapKey]);
                    memcpy(storedData, aggValue, sizeof(int));
                    _aggreMap[mapKey] = storedData;
                }
            }
        }
            break;
            
        case SUM:
        {
            if(_aggreMap.find(mapKey) == _aggreMap.end())
            {
                void * storedData = malloc(sizeof(int));
                memcpy(storedData, aggValue, sizeof(int));
                _aggreMap[mapKey] = storedData;
            }
            else
            {
                getSum(aggValue, _aggreMap[mapKey]);
            }
            break;
        }
            
        case AVG:
        {
            void *avgStoredData = malloc(sizeof(int));
            void *trueValue = malloc(2*sizeof(int));
            if(_aggreMap.find(mapKey) == _aggreMap.end())
            {
                count = 1;
                memcpy(avgStoredData, &count, sizeof(int));
                memcpy(trueValue, aggValue, sizeof(int));
                memcpy((char *)trueValue + sizeof(int), avgStoredData, sizeof(int));
                _aggreMap[mapKey] = trueValue;
            }
            else
            {
                void *tempDataValue = _aggreMap[mapKey];
                void *sumData = malloc(sizeof(int));
                memcpy(sumData, tempDataValue, sizeof(int));
                memcpy(avgStoredData, (char *)tempDataValue + sizeof(int), sizeof(int));
                getSum(aggValue, sumData);
                countPlusOne(count, avgStoredData);
                memcpy(trueValue, sumData, sizeof(int));
                memcpy((char *)trueValue + sizeof(int), avgStoredData, sizeof(int));
                free(tempDataValue);
                _aggreMap[mapKey] = trueValue;
                free(sumData);
            }
            free(avgStoredData);
            break;
        }
            
        default:
            break;
    }
    
    return 0;
}

string Aggregate::getPartitionName(unsigned index)
{
    return _groupAttr.name + "_" + _aggAttr.name + std::to_string(index);
}

RC Aggregate::getNextTuple(void *data)
{
    if(_getNextEnd)
    {
        return QE_EOF;
    }
    if(!_hasGroup)
    {
        void *tuple = malloc(MAX_TUPLE_SIZE);
        void *key = malloc(sizeof(int));
        void *operateValue = malloc(sizeof(int));
        int count = 0;
        switch (_op) {
            case COUNT:
                while(_itr->getNextTuple(tuple) != QE_EOF)
                {
                    count++;
                }
                memcpy(operateValue, &count, sizeof(int));
                break;
                
            case MIN:
            case MAX:
                if(_itr->getNextTuple(tuple) != QE_EOF)
                {
                    getAggregatedDataFromTuple(tuple, key, false);
                    initOperateValue(operateValue, key);
                }
                while(_itr->getNextTuple(tuple) != QE_EOF)
                {
                    getAggregatedDataFromTuple(tuple, key, false);
                    if(needChange(key, operateValue))
                    {
                        memcpy(operateValue, key, sizeof(int));
                    }
                }
                break;
                
            case AVG:
            case SUM:
                if(_itr->getNextTuple(tuple) != QE_EOF)
                {
                    getAggregatedDataFromTuple(tuple, key, false);
                    initOperateValue(operateValue, key);
                    if(_op == AVG)
                    {
                        count++;
                    }
                }
                while(_itr->getNextTuple(tuple) != QE_EOF)
                {
                    getAggregatedDataFromTuple(tuple, key, false);
                    getSum(key, operateValue);
                    if(_op == AVG)
                    {
                        count++;
                    }
                }
                if(_op == AVG)
                {
                    doAverageOnSum(operateValue, count);
                }
                
                break;
                
            default:
                break;
        }
        
        memcpy(data, operateValue, sizeof(int));
        
        free(tuple);
        free(key);
        free(operateValue);
        _getNextEnd = true;
    }
    else
    {
        // Group Aggregate
        if(_groupAttr.type == TypeVarChar)
        {
            if(_iteratorStr == _aggreMapStr.end())
            {
                _getNextEnd = true;
                destruct();
                return QE_EOF;
            }
            unsigned offset = _iteratorStr->first.length();
            char keyChar[offset+sizeof(int)];
            char firstVarchar[offset+1];
            
            strcpy(firstVarchar, _iteratorStr->first.c_str());
            
            memcpy(keyChar, &offset, sizeof(int));
            memcpy(keyChar + sizeof(int), firstVarchar, offset);
            
            offset += sizeof(int);
            memcpy(data, keyChar, offset);
            
            if(_op != AVG)
            {
                memcpy((char *)data + offset, _iteratorStr->second, sizeof(int));
            }
            else
            {
                void *trueValue = _iteratorStr->second;
                float sumFloat;
                if(_aggAttr.type == TypeInt)
                {
                    int sumInt;
                    memcpy(&sumInt, trueValue, sizeof(int));
                    sumFloat = (float)sumInt;
                }
                else
                {
                    memcpy(&sumFloat, trueValue, sizeof(float));
                }
                unsigned sumCount;
                memcpy(&sumCount, (char *)trueValue + sizeof(int), sizeof(int));
                float result = sumFloat / sumCount;
                memcpy((char *)data + offset, &result, sizeof(float));
                
            }
            _iteratorStr++;
        }
        else
        {
            if(_iteratorNum == _aggreMapNum.end())
            {
                _getNextEnd = true;
                destruct();
                return QE_EOF;
            }
            memcpy(data, &(_iteratorNum->first), sizeof(int));
            if(_op != AVG)
            {
                memcpy((char *)data + sizeof(int), _iteratorNum->second, sizeof(int));
            }
            else
            {
                void *trueValue = _iteratorNum->second;
                float sumFloat;
                if(_aggAttr.type == TypeInt)
                {
                    int sumInt;
                    memcpy(&sumInt, trueValue, sizeof(int));
                    sumFloat = (float)sumInt;
                }
                else
                {
                    memcpy(&sumFloat, trueValue, sizeof(float));
                }
                unsigned sumCount;
                memcpy(&sumCount, (char *)trueValue + sizeof(int), sizeof(int));
                float result = sumFloat / sumCount;
                memcpy((char *)data + sizeof(int), &result, sizeof(float));
            }
            _iteratorNum++;
        }
    }
    return 0;
}

void Aggregate::getAggregatedDataFromTuple(void *tuple, void *key, bool isGroupAttr)
{
    unsigned offset = 0;
    unsigned pos = isGroupAttr?_groupAttrPos:_attrPos;
    for(unsigned i = 0; i < pos; i++)
    {
        if(_allAttrs.at(i).type == TypeVarChar)
        {
            int stringLen;
            memcpy(&stringLen, (char *)tuple + offset, sizeof(int));
            offset += stringLen;
        }
        offset += sizeof(int);
    }
    if(_hasGroup && _groupAttr.type == TypeVarChar)
    {
        int stringLen = 0;
        memcpy(&stringLen, (char *)tuple + offset, sizeof(int));
        memcpy(key, (char *)tuple + offset, sizeof(int) + stringLen);
    }
    else
    {
        memcpy(key, (char *)tuple + offset, sizeof(int));
    }
}

void Aggregate::initOperateValue(void *operateValue, void *key)
{
    memcpy(operateValue, key, sizeof(int));
}

void Aggregate::readInt(void *key, void *value, int &keyInt, int &valueInt)
{
    memcpy(&keyInt, key, sizeof(int));
    memcpy(&valueInt, value, sizeof(int));
}

void Aggregate::readFloat(void *key, void *value, float &keyFloat, float &valueFloat)
{
    memcpy(&keyFloat, key, sizeof(float));
    memcpy(&valueFloat, value, sizeof(float));
}

bool Aggregate::needChange(void *key, void *operateValue)
{
    if(_inputInt)
    {
        int tempKey = 0, tempValue = 0;
        readInt(key, operateValue, tempKey, tempValue);
        if((_op == MIN && tempKey < tempValue) || (_op == MAX && tempKey > tempValue))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        float tempKey = 0.0, tempValue = 0.0;
        readFloat(key, operateValue, tempKey, tempValue);
        if((_op == MIN && tempKey < tempValue) || (_op == MAX && tempKey > tempValue))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}

void Aggregate::getSum(void *key, void *operateValue)
{
    if(_inputInt)
    {
        int tempKey = 0, tempValue = 0;
        readInt(key, operateValue, tempKey, tempValue);
        tempValue += tempKey;
        memcpy(operateValue, &tempValue, sizeof(int));
    }
    else
    {
        float tempKey = 0.0, tempValue = 0.0;
        readFloat(key, operateValue, tempKey, tempValue);
        tempValue += tempKey;
        memcpy(operateValue, &tempValue, sizeof(float));
    }
}

void Aggregate::countPlusOne(unsigned count, void *value)
{
    memcpy(&count, value, sizeof(int));
    count++;
    memcpy(value, &count, sizeof(int));
}

void Aggregate::doAverageOnSum(void *operateValue, int count)
{
    float tempAverage = 0.0;

    if(_inputInt)
    {
        int tempValue = 0;
        memcpy(&tempValue, operateValue, sizeof(int));
        tempAverage = (float)tempValue;
    }
    else
    {
        memcpy(&tempAverage, operateValue, sizeof(float));
    }
    
    tempAverage = tempAverage / count;
    memcpy(operateValue, &tempAverage, sizeof(float));
}

void Aggregate::getAttributes(vector<Attribute> &attrs) const
{
    attrs.clear();
    if(_hasGroup)
    {
        attrs.push_back(_groupAttr);
    }
    
    Attribute attribute = _allAttrs.at(_attrPos);
    
    std::stringstream s;
    s << getAggregateName(_op);
    s << "(";
    s << attribute.name;
    s << ")";
    
    attribute.name = s.str();
    if(_op == COUNT)
    {
        attribute.type = TypeInt;
    }
    else if(_op == AVG)
    {
        attribute.type = TypeReal;
    }
    
    attrs.push_back(attribute);
}

string Aggregate::getAggregateName(AggregateOp op) const
{
    switch (op)
    {
        case MIN:		return "MIN";
        case MAX:		return "MAX";
        case SUM:		return "SUM";
        case AVG:		return "AVG";
        case COUNT:		return "COUNT";
        default:		return "???";
    }
}

// ================================
// ================================
// ================================
// ================================
// ================================
// ================================
// =========== GHJoin =============
// ================================
// ================================
// ================================
// ================================
// ================================
// ================================

GHJoin::GHJoin(Iterator *leftIn,               // Iterator of input R
            Iterator *rightIn,               // Iterator of input S
            const Condition &condition,      // Join condition (CompOp is always EQ)
            const unsigned numPartitions     // # of partitions for each relation (decided by the optimizer)
      )
{
	_leftInput = leftIn;
	_rightInput = rightIn;
	_condition = condition;
	_numPartitions = numPartitions;

	_leftInput->getAttributes(_lhsAttributes);
	_rightInput->getAttributes(_rhsAttributes);
    getAttrNames();
    _rhsRbfmsi = 0;
    _rhsBuffer = malloc(MAX_TUPLE_SIZE);
    memset(_rhsBuffer, 0, MAX_TUPLE_SIZE);
    
    if(initRBFMFiles() != 0)
    {
        cout<<"GHJ init RBFMFiles err"<<endl;
        return;
    }
    
    void *tuple = malloc(MAX_TUPLE_SIZE);
    while(_leftInput->getNextTuple(tuple) != QE_EOF)
    {
        if(partitionData(true, tuple) != 0)
        {
            cout<<"GHJ partition data err"<<endl;
            return;
        }
        memset(tuple, 0, MAX_TUPLE_SIZE);
    }
    while(_rightInput->getNextTuple(tuple) != QE_EOF)
    {
        if(partitionData(false, tuple) != 0)
        {
            cout<<"GHJ partition data err"<<endl;
            return;
        }
        memset(tuple, 0 ,MAX_TUPLE_SIZE);
    }
    free(tuple);
    
    _currentLhsFileIndex = -1;
    _currentRhsFileIndex = -1;
    
    if(prepareNextLeftFile() != 0)
    {
        cout<<"GHJ prepare next left err"<<endl;
        return;
    }
    if(goToNextRightFile() != 0)
    {
        cout<<"GHJ go to next right err"<<endl;
        return;
    }
}

GHJoin::~GHJoin()
{

}

RC GHJoin::destruct()
{
    // Free table
    if(_hashTable.size() != 0)
    {
        // Clear data
        for(unsigned i = 0; i != _hashTable.size(); ++i)
        {
            for(unsigned j = 0; j != _hashTable.at(i)->size(); ++j)
            {
                free(_hashTable.at(i)->at(j));
            }
            delete _hashTable.at(i);
        }
    }
    
    // Delete partition files
    for(unsigned i = 0; i < _numPartitions; i++)
    {
        if(_rbfm->closeFile(*(_lhsFileHandles.at(i))) != 0)
        {
            return -1;
        }
        if(_rbfm->destroyFile(getPartitionName(true, i)) != 0)
        {
            return -1;
        }
        
        if(_rbfm->closeFile(*(_rhsFileHandles.at(i))) != 0)
        {
            return -1;
        }
        if(_rbfm->destroyFile(getPartitionName(false, i)) != 0)
        {
            return -1;
        }
    }
    
    if(_rhsRbfmsi != 0)
    {
        delete _rhsRbfmsi;
        _rhsRbfmsi = 0;
    }
    
    free(_rhsBuffer);
    for(unsigned i = 0; i < _lhsBuffer.size(); i++)
    {
        free(_lhsBuffer.at(i));
    }
    
    for(unsigned i = 0; i < _numPartitions; i++)
    {
        delete _lhsFileHandles.at(i);
        delete _rhsFileHandles.at(i);
    }
    
    return 0;
}

void GHJoin::getAttrNames()
{
    for(std::vector<Attribute>::const_iterator it = _lhsAttributes.begin();
        it != _lhsAttributes.end(); it++)
    {
        _lhsAttrNames.push_back(it->name);
    }
    for(std::vector<Attribute>::const_iterator it = _rhsAttributes.begin();
        it != _rhsAttributes.end(); it++)
    {
        _rhsAttrNames.push_back(it->name);
    }
}

RC GHJoin::initRBFMFiles()
{
    _rbfm = RecordBasedFileManager::instance();
    for(unsigned i = 0; i < _numPartitions; i++)
    {
        string lhsFileName = getPartitionName(true, i);
        FileHandle *lhsFilehandle = new FileHandle();
        if(_rbfm->createFile(lhsFileName)!=0 || _rbfm->openFile(lhsFileName, *lhsFilehandle)!=0)
        {
            return -1;
        }
        _lhsFileHandles.push_back(lhsFilehandle);
        
        string rhsFileName = getPartitionName(false, i);
        FileHandle *rhsFilehandle = new FileHandle();
        if(_rbfm->createFile(rhsFileName)!=0 || _rbfm->openFile(rhsFileName, *rhsFilehandle)!=0)
        {
            return -1;
        }
        _rhsFileHandles.push_back(rhsFilehandle);
    }
    return 0;
}

RC GHJoin::partitionData(bool isLeft, void *tuple)
{
    void * attributeData = malloc(MAX_ATTRIBUTE_SIZE);
    memset(attributeData, 0, MAX_ATTRIBUTE_SIZE);
    if(getAttribute((isLeft?_lhsAttributes:_rhsAttributes), tuple, attributeData,
                    (isLeft?_condition.lhsAttr:_condition.rhsAttr), _attrType) != 0)
    {
        return -1;
    }
    
    unsigned hashedValue = getHashValue(false, attributeData);
    
    free(attributeData);
    
    unsigned fileIndex = hashedValue % _numPartitions;
    
    // Write data
    RID rid;
    if(_rbfm->insertRecord(*(isLeft?_lhsFileHandles.at(fileIndex):_rhsFileHandles.at(fileIndex)), (isLeft?_lhsAttributes:_rhsAttributes), tuple, rid) != 0)
    {
        return -1;
    }
    
    return 0;
}

RC GHJoin::prepareNextLeftFile()
{
    if(_hashTable.size() != 0)
    {
        // Clear data
        for(unsigned i = 0; i != _hashTable.size(); ++i)
        {
            for(unsigned j = 0; j != _hashTable.at(i)->size(); ++j)
            {
                free(_hashTable.at(i)->at(j));
            }
            delete _hashTable.at(i);
        }
    }
    _hashTable.clear();
    
    for(unsigned i = 0; i != _numPartitions; ++i)
    {
        vector<void *> *innerVector = new vector<void*>();
        _hashTable.push_back(innerVector);
    }
    
    if((int)_numPartitions == ++_currentLhsFileIndex)
    {
        return 1;
    }
    
    // Read data in
    bool emptyData = true;
    while(emptyData)
    {
        RBFM_ScanIterator lhsRbfmsi;
        
        if(_rbfm->scan(*(_lhsFileHandles.at(_currentLhsFileIndex)), _lhsAttributes, "", NO_OP, NULL, _lhsAttrNames, lhsRbfmsi) != 0)
        {
            return -1;
        }
        RID rid;
        void *tuple = malloc(MAX_TUPLE_SIZE);
        memset(tuple, 0, MAX_TUPLE_SIZE);
        while(lhsRbfmsi.getNextRecord(rid, tuple) != RBFM_EOF)
        {
            emptyData = false;
            void * attributeData = malloc(MAX_ATTRIBUTE_SIZE);
            memset(attributeData, 0, MAX_ATTRIBUTE_SIZE);
            if(getAttribute(_lhsAttributes, tuple, attributeData,
                            _condition.lhsAttr, _attrType) != 0)
            {
                return -1;
            }
            unsigned hashedValue = getHashValue(true, attributeData);
            unsigned fileIndex = hashedValue % _numPartitions;
            void *tempTuple = malloc(MAX_TUPLE_SIZE);
            memset(tempTuple, 0, MAX_TUPLE_SIZE);
            memcpy(tempTuple, tuple, MAX_TUPLE_SIZE);
            _hashTable.at(fileIndex)->push_back(tempTuple);
            free(attributeData);
        }
        free(tuple);
        lhsRbfmsi.close();
        delete _lhsFileHandles.at(_currentLhsFileIndex);
        FileHandle *tempFileHandle = new FileHandle();
        if(_rbfm->openFile(getPartitionName(true, _currentLhsFileIndex), *tempFileHandle) != 0)
        {
            return -1;
        }
        _lhsFileHandles.erase(_lhsFileHandles.begin() + _currentLhsFileIndex);
        _lhsFileHandles.insert(_lhsFileHandles.begin() + _currentLhsFileIndex, tempFileHandle);
        if(emptyData)
        {
            if(++_currentLhsFileIndex == (int)_numPartitions)
            {
                return 1;
            }
        }
    }
    return 0;
}

RC GHJoin::goToNextRightFile()
{
    if(_rhsRbfmsi != 0)
    {
        _rhsRbfmsi->close();
        delete _rhsRbfmsi;
        _rhsRbfmsi = 0;
        
        delete _rhsFileHandles.at(_currentRhsFileIndex);
        
        FileHandle *tempFileHandle = new FileHandle();
        
        _rhsFileHandles.erase(_rhsFileHandles.begin() + _currentRhsFileIndex);
        _rhsFileHandles.insert(_rhsFileHandles.begin() + _currentRhsFileIndex, tempFileHandle);
        
        string tempName = getPartitionName(false, _currentRhsFileIndex);
        if(_rbfm->openFile(tempName, *(_rhsFileHandles.at(_currentRhsFileIndex))) != 0)
        {
            return -1;
        }
    }
    
    if((int)_numPartitions == ++_currentRhsFileIndex)
    {
        return 1;
    }
    
    _rhsRbfmsi = new RBFM_ScanIterator();
    
    if(_rbfm->scan(*(_rhsFileHandles.at(_currentRhsFileIndex)), _rhsAttributes, "", NO_OP, NULL, _rhsAttrNames, *_rhsRbfmsi) != 0)
    {
        return -1;
    }
    return 0;
}

unsigned GHJoin::getHashValue(bool isInner, void *key)
{
    
    unsigned int hashedValue;
    switch (_attrType)
    {
        case TypeInt:
        {
            int keyValueInt;
            memcpy(&keyValueInt, key, sizeof(int));
            hashedValue = (isInner?IndexManager::innerIntHash(keyValueInt):IndexManager::intHash(keyValueInt));
            break;
        }
            
        case TypeReal:
        {
            float keyValueFloat;
            memcpy(&keyValueFloat, key, sizeof(float));
            hashedValue = (isInner?IndexManager::innerRealHash(keyValueFloat):IndexManager::realHash(keyValueFloat));
            break;
        }
            
        case TypeVarChar:
        {
            int varCharLength;
            memcpy(&varCharLength, key, sizeof(int));
            char str[varCharLength+1];
            memcpy(str, (char *)key+sizeof(int), varCharLength);
            str[varCharLength] = '\0';
            hashedValue = (isInner?IndexManager::innerVarCharHash(str):IndexManager::varCharHash(str));
            break;
        }
            
        default:
            break;
    }
    
    return hashedValue;
}

string GHJoin::getPartitionName(bool isLeft, unsigned index)
{
    std::stringstream s;
    s << (isLeft?"left":"right");
    s << "_join";
    s << index;
    s << "_";
    s << (isLeft?(_condition.lhsAttr):(_condition.rhsAttr));
    
    return s.str();
}

int GHJoin::getEqualTuple()
{
    void *attributeData0 = malloc(MAX_ATTRIBUTE_SIZE);
    memset(attributeData0, 0, MAX_ATTRIBUTE_SIZE);
    if(getAttribute(_rhsAttributes, _rhsBuffer, attributeData0,
                    _condition.rhsAttr, _attrType) != 0)
    {
        return -1;
    }
    size_t compareLength = sizeof(int);
    if(_attrType == TypeVarChar)
    {
        compareLength += *((int*)attributeData0);
    }
    
    unsigned hashedValue = getHashValue(true, attributeData0);
    unsigned fileIndex = hashedValue % _numPartitions;
    vector<void *> *tuples = _hashTable.at(fileIndex);
    void *attributeData1 = malloc(MAX_ATTRIBUTE_SIZE);
    for(unsigned i = 0; i != tuples->size(); ++i)
    {
        void * lhsTuple = malloc(MAX_TUPLE_SIZE);
        memcpy(lhsTuple, tuples->at(i), MAX_TUPLE_SIZE);
        memset(attributeData1, 0, MAX_ATTRIBUTE_SIZE);
        if(getAttribute(_lhsAttributes, lhsTuple, attributeData1,
                        _condition.lhsAttr, _attrType) != 0)
        {
            return -1;
        }
        if(memcmp(attributeData0, attributeData1, compareLength) == 0)
        {
            _lhsBuffer.push_back(lhsTuple);
        }
        else
        {
            free(lhsTuple);
        }
    }
    free(attributeData0);
    free(attributeData1);
    return 0;
}

void GHJoin::getDataFromBuffer(void *data)
{
    void *lhsData = _lhsBuffer.front();
    combineTuple(_lhsAttributes, _rhsAttributes, lhsData, _rhsBuffer, data);
    free(lhsData);
    _lhsBuffer.erase(_lhsBuffer.begin());
}

RC GHJoin::getNextTuple(void *data)
{
    if(!_lhsBuffer.empty())
    {
        getDataFromBuffer(data);
        return 0;
    }
    
    do
    {
        RID rid;
        void *tuple = malloc(MAX_TUPLE_SIZE);
        memset(tuple, 0, MAX_TUPLE_SIZE);
        while(_rhsRbfmsi->getNextRecord(rid, tuple) != RBFM_EOF)
        {
            int rhsTupleLength = _rbfm->getRecordLengthFromDescriptor(_rhsAttributes, tuple);
            memcpy(_rhsBuffer, tuple, rhsTupleLength);
            getEqualTuple();
            if(!_lhsBuffer.empty())
            {
                break;
            }
        }
        free(tuple);
        if(_lhsBuffer.empty())
        {
            int res = goToNextRightFile();
            if(res == -1)
            {
                return -1;
            }
            else if(res == 1)
            {
                _currentRhsFileIndex = -1;
                if(goToNextRightFile() != 0)
                {
                    return -1;
                }
                int innerRes = prepareNextLeftFile();
                if(innerRes == -1)
                {
                    return -1;
                }
                else if(innerRes == 1)
                {
                    if(destruct() != 0)
                    {
                        return -1;
                    }
                    return QE_EOF;
                }
            }
        }
    } while(_lhsBuffer.empty());
    
    if(!_lhsBuffer.empty())
    {
        getDataFromBuffer(data);
        return 0;
    }
    return 0;
}

// For attribute in vector<Attribute>, name it as rel.attr
void GHJoin::getAttributes(vector<Attribute> &attrs) const
{
	getAttributesAfterJoin(_lhsAttributes, _rhsAttributes, attrs);
}

// ================
// ==== BNLJoin ===
// ================
BNLJoin::BNLJoin(Iterator *leftIn,            // Iterator of input R
               TableScan *rightIn,           // TableScan Iterator of input S
               const Condition &condition,   // Join condition
               const unsigned numRecords     // # of records can be loaded into memory, i.e., memory block size (decided by the optimizer)
        )
{
	this->leftInput = leftIn;
	this->rightInput = rightIn;
	this->condition = condition;
	this->numRecords = numRecords;
	this->isFirstQuery = true;

	leftInput->getAttributes(lhsAttributes);
	rightInput->getAttributes(rhsAttributes);

	rhsTupleData = malloc(MAX_TUPLE_SIZE);
}

BNLJoin::~BNLJoin()
{
	freeCurrentBlock();
	free(rhsTupleData);
}

RC BNLJoin::loadNextBlock()
{
	freeCurrentBlock();

	void *lhsTupleData = malloc(MAX_TUPLE_SIZE);
	memset(lhsTupleData, 0, MAX_TUPLE_SIZE);
	
	if(leftInput->getNextTuple(lhsTupleData) == QE_EOF)
	{
		free(lhsTupleData);
		return QE_EOF;
	}

	lhsBlockOfTuple.push_back(lhsTupleData);

	for(unsigned i=1; i<numRecords; i++)
	{
		lhsTupleData = malloc(MAX_TUPLE_SIZE);
		memset(lhsTupleData, 0, MAX_TUPLE_SIZE);	

		if(leftInput->getNextTuple(lhsTupleData) == QE_EOF)
		{
			free(lhsTupleData);
			break;
		}

		lhsBlockOfTuple.push_back(lhsTupleData);
	}


	lhsTupleIterator = 0;

	return 0;
}

RC BNLJoin::freeCurrentBlock()
{
	while(lhsBlockOfTuple.size()!=0)
	{
		free(lhsBlockOfTuple.front());
		lhsBlockOfTuple.erase(lhsBlockOfTuple.begin());
	}

	return 0;
}

RC BNLJoin::getNextTuple(void *data)
{
	if(isFirstQuery == true)
	{
		memset(rhsTupleData, 0, MAX_TUPLE_SIZE);

		if((loadNextBlock() == QE_EOF) || (rightInput->getNextTuple(rhsTupleData) == QE_EOF))
		{
			return QE_EOF;
		}
	}

	if(lhsBlockOfTuple.size() == 0)
	{
		return QE_EOF;
	}

	do
	{
		//If reach the end of block, try to load next tuple from table scan
		if(lhsTupleIterator == lhsBlockOfTuple.size())
		{
			//If reach the end of rhs, try to load next block
			memset(rhsTupleData, 0, MAX_TUPLE_SIZE);
			if(rightInput->getNextTuple(rhsTupleData) == QE_EOF)
			{
				//If reach the end of lhs, return.
				if(loadNextBlock() == QE_EOF)
				{
					return QE_EOF;
				}
				else
				{
					rightInput->setIterator();
					rightInput->getNextTuple(rhsTupleData);
				}
			}

			//Reset the iterator of block.
			lhsTupleIterator = 0;
		}

		lhsTupleData = lhsBlockOfTuple[lhsTupleIterator];
		lhsTupleIterator++;
	}
	while(isSatisfied()==false);

	combineTuple(lhsAttributes, rhsAttributes, lhsTupleData, rhsTupleData, data);

	isFirstQuery = false;

	return 0;
}

bool BNLJoin::isSatisfied()
{
	void *lhsAttrData = malloc(MAX_ATTRIBUTE_SIZE);
	memset(lhsAttrData, 0, MAX_ATTRIBUTE_SIZE);
	void *rhsAttrData = malloc(MAX_ATTRIBUTE_SIZE);
	memset(rhsAttrData, 0, MAX_ATTRIBUTE_SIZE);
	
	getAttribute(lhsAttributes, lhsTupleData, lhsAttrData, condition.lhsAttr, attrType);
	getAttribute(rhsAttributes, rhsTupleData, rhsAttrData, condition.rhsAttr, attrType);

	int compLength = sizeof(int);
	if(attrType == 2)
	{
		compLength += *(int *)lhsAttrData;
	}

	bool result;
	result = (memcmp(lhsAttrData, rhsAttrData, compLength) == 0)? true: false;

	free(lhsAttrData);
	free(rhsAttrData);

	return result;
}

// For attribute in vector<Attribute>, name it as rel.attr
void BNLJoin::getAttributes(vector<Attribute> &attrs) const
{
	getAttributesAfterJoin(lhsAttributes, rhsAttributes, attrs);
}

// ================
// ==== INLJoin ===
// ================
INLJoin::INLJoin(Iterator *leftIn,           // Iterator of input R
               IndexScan *rightIn,          // IndexScan Iterator of input S
               const Condition &condition   // Join condition
        )
{
	this->leftInput = leftIn;
	this->rightInput = rightIn;
	this->condition = condition;
	this->isFirstQuery = true;

	leftInput->getAttributes(lhsAttributes);
	rightInput->getAttributes(rhsAttributes);

	lhsTupleData = malloc(MAX_TUPLE_SIZE);
	rhsTupleData = malloc(MAX_TUPLE_SIZE);
}

INLJoin::~INLJoin()
{
	free(lhsTupleData);
	free(rhsTupleData);
}

RC INLJoin::getNextTuple(void *data)
{
	//Try to get next tuple from indexScan
	memset(rhsTupleData, 0, MAX_TUPLE_SIZE);
	if((isFirstQuery == false) && (rightInput->getNextTuple(rhsTupleData) != QE_EOF))
	{
		combineTuple(lhsAttributes, rhsAttributes, lhsTupleData, rhsTupleData, data);

		return 0;
	}

	//If reach the end, try to get next tuple from Iterator
	memset(lhsTupleData, 0, MAX_TUPLE_SIZE);
	do
	{
		if(leftInput->getNextTuple(lhsTupleData) == QE_EOF)
		{
			return QE_EOF;
		}

		setIndexScanIterator(lhsTupleData);
	}
	while(rightInput->getNextTuple(rhsTupleData) == QE_EOF);

	combineTuple(lhsAttributes, rhsAttributes, lhsTupleData, rhsTupleData, data);

	isFirstQuery = false;

	return 0;
}

RC INLJoin::setIndexScanIterator(const void *lhsTupleData)
{
	void *lhsAttrData = malloc(MAX_ATTRIBUTE_SIZE);
	memset(lhsAttrData, 0, MAX_ATTRIBUTE_SIZE);

	getAttribute(lhsAttributes, lhsTupleData, lhsAttrData, condition.lhsAttr, attrType);
	rightInput->setIterator(lhsAttrData, lhsAttrData, true, true);
	
	free(lhsAttrData);

	return 0;
}

// For attribute in vector<Attribute>, name it as rel.attr
void INLJoin::getAttributes(vector<Attribute> &attrs) const
{
	getAttributesAfterJoin(lhsAttributes, rhsAttributes, attrs);
}


// ================
// === Iterator ===
// ================
RC Iterator::getAttribute(const vector<Attribute> &attributes, const void *tupleData, void *attributeData, string &attrName, AttrType &attrType)
{
	RC rc = RecordBasedFileManager::instance()->readAttributeFromRecord(attributes, attrName, tupleData, attributeData);

	if(rc != -1)
	{
		attrType = attributes[rc].type;

		return 0;
	}
	else
	{
		return -1;
	}
}

RC Iterator::combineTuple(const vector<Attribute> &lhsAttributes, const vector<Attribute> &rhsAttributes, const void *lhsTupleData, const void *rhsTupleData, void *combineTupleData)
{
	RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();

	int lhsTupleLength = rbfm->getRecordLengthFromDescriptor(lhsAttributes, lhsTupleData);
	int rhsTupleLength = rbfm->getRecordLengthFromDescriptor(rhsAttributes, rhsTupleData);

	memset(combineTupleData, 0, lhsTupleLength + rhsTupleLength);
	memcpy(combineTupleData, lhsTupleData, lhsTupleLength);
	memcpy((char *)combineTupleData + lhsTupleLength, rhsTupleData, rhsTupleLength);

	return 0;
}

RC Iterator::getAttributesAfterJoin(const vector<Attribute> &lhsAttributes, const vector<Attribute> &rhsAttributes, vector<Attribute> &attrs) const
{
	attrs.clear();

	for(int i=0; i<(int)lhsAttributes.size(); i++)
	{
        attrs.push_back(lhsAttributes[i]);
	}

	for(int i=0; i<(int)rhsAttributes.size(); i++)
	{
		attrs.push_back(rhsAttributes[i]);
	}

	return 0;
}